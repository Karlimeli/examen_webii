const fs = require("fs");
const express  = require("express");
const cors = require('cors');

const path = require("path");

const PUERTO= 3000;

const home = fs.readFileSync("./index.html");

const server  = express();

server.use(cors()).use(express.json());

server.get("/", devolverIndex );

server.listen(PUERTO,()=>{
    console.log(`El servidor esta ejecutAndose por el puerto ${PUERTO}`);
})

const paginaDeError = path.join(__dirname, './error.html' );

server.use( (req,res,next) =>{
    res.status(404).sendFile(paginaDeError);
})

function devolverIndex(req,res)
{
    res.write(home);
    res.end();
}
