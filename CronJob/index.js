const moongose= require("mongoose")
const axios =require("axios").default;
const cheerio = require("cheerio");
const cron =require("node-cron");

const {MONGO_URI}= require('./config');
const {Cita} = require ("./models");

moongose.connect(MONGO_URI, {useNewUrlParser: true, useUnifiedTopology:true});

cron.schedule("2 * * * *",
//1. conectarnos a la pagina web de cnn en español
async ()=> {

    const html = await axios.get('index.html');
    //2 cargar el html de la pagina para poder trabajar con él
    const $ = cheerio.load(html.data);
    const datos = $(".titulos");
    let arregloCitas=[]
    
    //3. filtrar del html solo las noticias
    datos.each((index, element)=>{
        //4. guardar las noticias en la base de datos
        const ObjetoCita=
        {
            titulo: $(element).text(),
            dato: $(element).children()
        }
        arregloCitas =[...arregloCitas, ObjetoCita];
    })
    Cita.create(arregloCitas);
    //5. que esta informacion se guarde cada cierto tiempo definido por nosotros
}
)