const mongoose =require("mongoose");
const {Schema} = mongoose;

const CitaSchema =
new Schema(
{
    Doctor: { type:String },
    Paciente: { type:String },
    Motivo: { type:String },
    FechaCita: { type:String },
    ProximaCita: { type:String },
    Prioridad: { type:String },
    TipoError: { type:String },
},    
    {
        timestamps:{ createdAt: true, updateAt:true }
    }
);

module.exports = mongoose.model("Cita", CitaSchema );