const express= require("express");

const app= express();

const PORT=3000;

let arreglo=[];

//exponer carpeta con recursos estáticos
app.use('/public',express.static( __dirname+'/public'));
app.use(express.json())


/// http://localhost:3000/api/v1/estudiantes

// estudiantes
///    /
app.get('/', (req, res)=>{
    res.send(arreglo);
})


/// get
///    /individual/:nombre
app.get('/:Prioridad', (req, res)=>{
    const {Prioridad} =req.params;
    const respuesta= arreglo.filter(p=>{
        return p.Prioridad===Prioridad
    })
    if (respuesta != 1)
    {
        res.status(200).send({
            respuesta:"No se puede agendar una próxima cita"
        })
    }
    else
    {
        res.status(404).send({
            respuesta:"No se pudo encontrar el elemento"
        })

    }
})

/// get
///    /


/// post

app.post('/', (req,res)=>{
    const{body}=req;
    arreglo.push(body);
    res.send(
        {
            mensaje:"Se almacenó correctamente",
            respuesta:body
        }
    );      
})




app. listen(PORT, ()=>{
    console.log(`El servidor esta escuchando por el puerto ${PORT}`)
})
